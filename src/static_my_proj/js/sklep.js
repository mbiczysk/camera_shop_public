$(window).on('load', function(){
    //
    //Scrolled description in products card
    var $descriptionItems = $(".description")
    var shouldBeHeight = 300
    $.each($descriptionItems, function(index, obj){

        var item = $(this)
        var actualHeight = item[0].scrollHeight

        if (actualHeight > shouldBeHeight){
            item.css("height", shouldBeHeight);
            item.mCustomScrollbar({
                theme:"minimal-dark"
            });
        }
    })

    //
    // random bottom carousel
    var $bottom_car_item = $('.carousel-item.car_bottom');
    var $numberofSlides = $bottom_car_item.length;
    var $currentSlide = Math.floor((Math.random() * $numberofSlides));
    $bottom_car_item.eq($currentSlide).addClass('active');



    //
    // product detail view gallery
    $('#prodGallery').carousel({
        interval: 100000
    });

    // handles the carousel thumbnails
    $('[id^=carousel-selector-]').click( function(){
      var id_selector = $(this).attr("id");
      var id = id_selector.substr(id_selector.length -1);
      id = parseInt(id);
      $('#prodGallery').carousel(id);
      $('[id^=carousel-selector-]').removeClass('img_selected');
      $(this).addClass('img_selected');
    });

    // when the carousel slides, auto update
    $('#prodGallery').on('slid.bs.carousel', function (e) {
      var id = $('.carousel-item.active').data('slide-number');
      id = parseInt(id);
      $('[id^=carousel-selector-]').removeClass('img_selected');
      $('[id=carousel-selector-'+id+']').addClass('img_selected');
    });


    $('[id^=imgSmall]').click(function(){
    var img_src = $(this).attr("src");
    $("#imgBig").attr("src",img_src);
    $("#overlay").show();
    $("#overlayContent").show();
    });

    $("#imgBig").click(function(){
    $("#imgBig").attr("src", "");
    $("#overlay").hide();
    $("#overlayContent").hide();
    });



    // navbar smaller height on scroll
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= 5) { // use any value lower than the navbar height, [20] is an example

            $('.navbar').css({ // reducing the vertical padding from 25px to 10px
                'opacity': 0.6,
                'padding-top': '0px',
                'padding-bottom': '0px',
                'transition': '0.5s all',
            });

        } else {

            $('.navbar').css({ // reset the vertical padding to its initial value [25px]
                'opacity': 1,
                'padding-top': '15px',
                'padding-bottom': '15px'
            });

        }
    });



});


