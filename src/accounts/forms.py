from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class UserDataUpdateForm(forms.ModelForm):
    username = forms.CharField(
        label='Nazwa użytkownika',
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }),
            required=False
    )

    class Meta:
        model = User
        fields = ['username',]


class GuestForm(forms.Form):
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control",
                "placeholder": "E-Mail",
            }
        ),
        label="Twój E-Mail"
    )

    subscribe = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                "style": "width: 15px; height: 15px; margin: 5px 6px 6px;"
            }
        ),
        label="Subskrypcja newslettera"
    )

class LoginForm(forms.Form):
    username = forms.CharField(
        label="Twój Login",
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Login",
            }))

    password = forms.CharField(label="Twoje hasło", widget=forms.PasswordInput(attrs={
                "class": "form-control",
                "placeholder": "Hasło",
            }))

    def clean_username(self):
        data = self.cleaned_data
        username = data.get('username')
        qs = User.objects.filter(username=username)
        if not qs.exists():
            raise forms.ValidationError("Niepoprawny użytkownik")
        return username




class RegisterForm(forms.Form):
    username = forms.CharField(label="Twój Login", widget=forms.TextInput(
        attrs={
            "class": "form-control",
            "placeholder": "Nazwa",
        }))

    password = forms.CharField(label="Twoje hasło", widget=forms.PasswordInput(attrs={
        "class": "form-control",
        "placeholder": "Hasło",
    }))

    password2 = forms.CharField(label="Potwierdź hasło", widget=forms.PasswordInput(attrs={
        "class": "form-control",
        "placeholder": "Hasło"
    }))

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control",
                "placeholder": "E-Mail",
            }
        ),
        label="Twój E-Mail"
    )

    subscribe = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                "style": "width: 15px; height: 15px; margin: 5px 6px 6px;"
            }
        ),
        label="Subskrypcja newslettera",
    )

    def clean_username(self):
        data = self.cleaned_data
        username = data.get('username')
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError("Użytkownik już istnieje")
        return username

    def clean_password2(self):
        data = self.cleaned_data
        password = data.get('password')
        password2 = data.get('password2')
        if password2 != password:
            raise forms.ValidationError("Niezgodność podanych haseł")
        return data

    def clean_email(self):
        data = self.cleaned_data
        email = data.get('email')
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError("Email znajduje się już w bazie")
        return email
