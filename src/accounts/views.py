from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic import DetailView, FormView, UpdateView
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from django.utils.safestring import mark_safe

from .forms import LoginForm, RegisterForm, GuestForm, UserDataUpdateForm
from .models import GuestEmail
from orders.models import Order
from marketing.models import MarketingPref
from addresses.models import Address
from mixins import NextUrlMixin
from products.models import Product

User = get_user_model()


class SettingsPageView(LoginRequiredMixin, DetailView, UpdateView):
    template_name = "accounts/settings.html"
    redirect_field_name = "next_url"
    form_class = UserDataUpdateForm
    success_url = '#'

    # Newsletter

    def get_object(self):
        return self.request.user

    def get_context_data(self, *args, **kwargs):
        request = self.request
        user = request.user
        context = super(SettingsPageView, self).get_context_data(*args, **kwargs)

        user_orders = Order.objects.user_orders_history(user)
        addresses_qs = Address.objects.filter(billing_profile__email=user.email, current=True)
        billing_addresses = addresses_qs.filter(address_type="billing")
        shipping_addresses = addresses_qs.filter(address_type="shipping")
        unique_viewed_products = self.get_viewed_products(user)

        context['orders'] = user_orders
        context['billing_addresses'] = billing_addresses
        context['shipping_addresses'] = shipping_addresses
        context['viewed_products'] = unique_viewed_products

        return context

    def get_viewed_products(self, user):
        viewed_history = user.objectviewed_set.viewed_products()
        viewed_products_ids = [x.object_id for x in viewed_history]
        # viewed_products = Product.objects.filter(id__in=viewed_products_ids)  # no chronology!
        # viewed_products = [Product.objects.get(id=prod_id) for prod_id in viewed_products_ids]  # proper chronology but no exception handling

        # only proper solution
        viewed_products = []
        for prod_id in viewed_products_ids:
            try:
                viewed_products.append(Product.objects.get(id=prod_id))
            except Product.DoesNotExist:
                pass

        unique_viewed_products = Product.make_qs_unique(viewed_products)

        return unique_viewed_products[:10]

    def post(self, request):
        if request.POST.get('del_address'):
            address_id = request.POST.get('address_id')
            qs = Address.objects.filter(id=address_id)
            if qs:
                address_to_del = qs.first()
                address_to_del.make_stale()
        return redirect('accounts:settings')


class GuestRegisterView(NextUrlMixin, FormView):
    form_class = GuestForm

    def get_context_data(self, **kwargs):
        context = super(GuestRegisterView, self).get_context_data(**kwargs)
        context['title'] =  "Kontynuuj jako gość"
        context['button'] = "Dalej"
        return context

    def form_invalid(self, form):
        request = self.request
        messages.warning(request, 'Formularz został wypełniony niepoprawnie!')
        return super(GuestRegisterView, self).form_invalid(form)

    def form_valid(self, form):
        request = self.request

        email = form.cleaned_data.get("email")
        new_guest_email = GuestEmail.objects.create(email=email)
        request.session['guest_email_id'] = new_guest_email.id

        # Subscribe to newsletter
        newsletter_subscribe(request, form, guest_email=new_guest_email.email)

        next_path = self.get_next_url()
        return redirect(next_path)


class LoginView(NextUrlMixin, FormView):
    form_class = LoginForm
    template_name = "accounts/auth.html"

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['title'] = "Zaloguj się na konto"
        context['button'] = "Zaloguj"
        context['login'] = "True"
        return context

    def form_invalid(self, form):
        request = self.request
        messages.warning(request, 'Formularz został wypełniony niepoprawnie!')
        return super(LoginView, self).form_invalid(form)

    def form_valid(self, form):
        request = self.request
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user_auth = authenticate(request, username=username, password=password)
        user_obj = User.objects.get(username=username)

        if user_obj.is_active == True:

            if user_auth is not None:
                login(request, user_auth)
                messages.success(request, 'Pomyślnie zalogowano użytkownika {}'.format(username))
                request.session['username'] = username

                if request.session.get('guest_email_id'):
                    del request.session['guest_email_id']

                next_path = self.get_next_url()
                return redirect(next_path)

            else:
                messages.warning(request, 'Niepoprawne hasło!')
                return self.form_invalid(form)

        else:
            messages.warning(
                request,
                'Konto nieaktywne, czy dokonałeś aktywacji? Sprawdź instrukcję wysłaną na adres e-mail: {}'.format(
                    user_obj.email)
            )

            reactivate_url = f'/accounts/reactivate/{user_obj.username}/{user_obj.email}/'

            messages.warning(
                request,
                mark_safe(
                    f"Kliknij <a href='{reactivate_url}'>TUTAJ</a> aby ponownie przesłać link aktywacyjny."
                )
            )

            return self.form_invalid(form)


def logout_page(request):
    if request.session.get('guest_email_id'):
        del request.session['guest_email_id']
        messages.success(request, 'Zakończono sesję gościa')
    else:
        logout(request)
        messages.success(request, 'Pomyślnie wylogowano użytkownika')

    if request.session.get('cart_id'):
        del request.session['cart_id']
    if request.session.get('cart_items_count'):
        del request.session['cart_items_count']

    return redirect('/')


class RegisterView(NextUrlMixin, FormView):
    form_class = RegisterForm
    template_name = "accounts/auth.html"
    default_next = 'accounts:login'

    def get_context_data(self, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context['title'] = "Utwórz nowe konto"
        context['button'] = "Zarejestruj"
        return context

    def form_invalid(self, form):
        request = self.request
        messages.warning(request, 'Formularz został wypełniony niepoprawnie!')
        return super(RegisterView, self).form_invalid(form)

    def form_valid(self, form):
        request = self.request
        user = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        new_user = User.objects.create_user(user, email, password)
        new_user.is_active = False
        new_user.save()

        messages.info(
            request,
            'Utworzono konto dla użytkownika {}, aktywuj swoje konto postępując zgodnie z instrukcjami wysłanymi na adres e-mail {}'.format(
                user, email)
        )

        # Subscribe to newsletter
        newsletter_subscribe(request, form, new_user=new_user)

        # Send activation mail
        send_activation_mail(request, user, email)

        next_path = self.get_next_url()
        return redirect(next_path)


def newsletter_subscribe(request, form, new_user=None, guest_email=None):
    newsletter_sub = form.cleaned_data.get('subscribe')
    if newsletter_sub:

        if guest_email:
            MarketingPref.objects.get_or_create(user=None, guest_user_email=guest_email)

        elif new_user:
            MarketingPref.objects.get_or_create(user=new_user)

        messages.success(request, 'Zostałeś dodany do naszego Newslettera!')


def send_activation_mail(request, user, email):
    new_user = User.objects.get(username=user)

    mail_subject = "Aktywuj swoje konto w naklisze.pl"
    current_site = get_current_site(request)
    message = render_to_string('accounts/acc_active_email.html', {
        'user': new_user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
        'token': account_activation_token.make_token(new_user),
    })
    email_msg = EmailMessage(
        mail_subject, message, to=[email]
    )
    try:
        email_msg.send()
    except:
        messages.warning(request, 'Nastąpił błąd przy wysyłaniu wiadomości aktywacyjnej')

    return redirect("accounts:login")


def activation_view(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user_obj = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user_obj = None

    if user_obj and account_activation_token.check_token(user_obj, token):
        user_obj.is_active = True
        user_obj.save()
        messages.success(request, 'Aktywowano konto użytkownika {}, możesz się już zalogować'.format(user_obj.username))
        return redirect('accounts:login')

    else:
        messages.warning(request, 'Niepoprawny link aktywacji konta!')
        return redirect('accounts:register')
