from django.db import models
from django.conf import settings

from django.db.models.signals import pre_save, post_save, m2m_changed

from products.models import Product
User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):

    def new_or_get(self, request):
        cart_id = request.session.get("cart_id", None)
        # qs = Cart.objects.filter(id=cart_id)
        qs = self.get_queryset().filter(id=cart_id)

        if qs.count() == 1:
            new_obj = False
            cart_obj = qs.first()
            if request.user.is_authenticated() and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
        else:
            new_obj = True
            cart_obj = self.new(user=request.user)
            request.session['cart_id'] = cart_obj.id

        return cart_obj, new_obj

    def new(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated():
                user_obj = user

        return self.model.objects.create(user=user_obj)


class Cart(models.Model):
    user                = models.ForeignKey(User, null=True, blank=True)
    products            = models.ManyToManyField(Product, blank=True)
    total               = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    subtotal            = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    timestamp           = models.DateTimeField(auto_now_add=True)
    updated             = models.DateTimeField(auto_now=True)
    shipping_cost       = models.DecimalField(default=20.00, max_digits=10, decimal_places=2)


    objects = CartManager()

    def __str__(self):
        return  str(self.id)

    def product_list(self):
        return [product.id for product in self.products.all()]


def m2m_changed_cart_receiver(sender, instance, action, *args, **kwargs):
    if action in ['post_remove', 'post_add', 'post_clear']:
        products = instance.products.all()
        subtotal = sum([product.promo_price if (product.promo_price and product.promo_active) else product.price for product in products])
        instance.subtotal = subtotal
        instance.save()

m2m_changed.connect(m2m_changed_cart_receiver, sender=Cart.products.through)


def pre_save_cart_receiver(sender, instance, *args, **kwargs):
    if instance.subtotal > 0:
        instance.total = instance.subtotal  + instance.shipping_cost
    else:
        instance.total = 0.00

pre_save.connect(pre_save_cart_receiver, sender=Cart)