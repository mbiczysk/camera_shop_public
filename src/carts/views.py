from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib import messages
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.db.models import Q

from .models import Cart
from products.models import Product
from orders.models import Order
from accounts.forms import LoginForm, GuestForm
from billing.models import BillingProfile
from addresses.forms import AddressForm
from addresses.models import Address


def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = cart_obj.products.all()

    context = {
        'total_price': cart_obj.subtotal,
        'basket_products': products,
        'cart': cart_obj
    }

    return render(request, "carts/home.html", context)


def cart_update(request):
    product_id = request.POST.get('product')
    add_another_multiple = request.POST.get('add_another_multiple')

    if product_id is not None:
        try:
            product_obj = Product.objects.get(id=product_id)

        except Product.DoesNotExist:

            messages.warning(request, 'Wystąpił błąd, produkt nie istnieje w bazie')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        cart_obj, new_obj = Cart.objects.new_or_get(request)

        # try add to cart another instance of same type multiple product
        if add_another_multiple:
            qs = get_another_multiple_product(product_obj, cart_obj)

            # if there is available another product -> add to cart
            if qs:
                new_product = qs[0]
                cart_obj.products.add(new_product)
            else:
                messages.warning(request, 'Brak dostępnych kolejnych produktów tego typu')

        else:
            if product_obj in cart_obj.products.all():
                cart_obj.products.remove(product_obj)
            else:
                cart_obj.products.add(product_obj)  # cart_obj.products.add(product_id)
                messages.success(request, 'Produkt został dodany do koszyka')

        request.session['cart_items_count'] = cart_obj.products.count()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def checkout_home(request):

    cart_obj, cart_created = Cart.objects.new_or_get(request) # create or get cart
    products = cart_obj.products.all()

    order_obj = None
    address_qs = None

    login_form = LoginForm()
    guest_form = GuestForm()
    address_form = AddressForm()

    billing_address_id = request.session.get("billing_address_id", None)
    shipping_address_id = request.session.get("shipping_address_id", None)

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)

    if request.session.get('success_order_id'):
        del request.session['success_order_id']

    if cart_created or cart_obj.products.count() == 0: # new empty cart or no products in cart so no checkout
        return redirect("carts:home")

    if billing_profile is not None: # Billing profile is available so we can create or update order
        order_obj, order_obj_created = Order.objects.new_or_get(cart_obj=cart_obj, billing_profile=billing_profile)
        address_qs = Address.objects.filter(billing_profile=billing_profile, current=True)

        # make_checkout flag is received via GET trough check_availability
        make_checkout = request.GET.get('make_checkout')

        del_shipping_address = request.POST.get('del_shipping_address')
        del_billing_address = request.POST.get('del_billing_address')

        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
            order_obj.save()
            del request.session["shipping_address_id"]

        if billing_address_id:
            order_obj.billing_address = Address.objects.get(id=billing_address_id)
            order_obj.save()
            del request.session["billing_address_id"]

        if make_checkout:
            order_is_done = order_obj.check_if_done()
            if order_is_done:

                order_status = order_obj.mark_as_done()
                request.session['success_order_id'] = order_obj.order_id
                return redirect("carts:checkout_success")

        # delete current address from order and go back to choose another one
        if del_shipping_address or del_billing_address:
            if del_shipping_address:
                order_obj.shipping_address = None
                order_obj.save()
            elif del_billing_address:
                order_obj.billing_address = None
                order_obj.save()

            return redirect("carts:checkout")

    context = {
        "object": order_obj,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "basket_products": products,
        "address_qs": address_qs

    }

    return render(request, "carts/checkout.html", context)

def checkout_success(request):
    success_order_id = request.session.get('success_order_id')
    order_obj = Order.objects.get(order_id=success_order_id)
    success_user_email = order_obj.billing_profile.email
    success_order_total = str(order_obj.total)
    context = {
        'success_order_id': success_order_id,
        'success_user_email': success_user_email,
        'success_order_total': success_order_total
    }


    bought_products = order_obj.cart.products.all()
    for product in bought_products:
        product.make_selled()

    # Send order success mail
    send_order_success_mail(request, success_order_id, success_user_email, success_order_total, bought_products, order_obj)

    if request.session.get('cart_id'):
        del request.session['cart_id']
    if request.session.get('cart_items_count'):
        del request.session['cart_items_count']
    if request.session.get('guest_email_id'):
        del request.session['guest_email_id']
        messages.info(request, 'Zakończono sesję gościa')

    return render(request, "carts/checkout_success.html", context)


def get_another_multiple_product(product, cart_obj):
    # if possible return qs of available instances of same model of 'multiple type' product

    product_category = product.category
    product_model = product.model  # model is unique type field

    # constructing Q query :)
    queryClass = product.__class__.__name__
    queryField = 'model'
    queryFilter = queryClass + '___' + queryField

    # unpacking dict to kwargs hack
    qs = Product.objects.all_available().filter(category=product_category).filter(
        Q(**{queryFilter: product_model}))

    # filter out products which are already in cart
    qs = [prod for prod in qs if not prod in cart_obj.products.all()]

    return qs

def check_availability(request):
    next_get_url = request.GET.get('next_url')
    next_post_url = request.POST.get('next_url')
    make_checkout = request.POST.get('make_checkout')
    redirect_path = (next_post_url or next_get_url) or None

    cart_obj, cart_created = Cart.objects.new_or_get(request)  # create or get cart
    products = cart_obj.products.all()
    all_available = True

    for product in products:
        if product.selled == True:

            # try replace 'multiple' product with available one
            if product.category in product.MULTIPLE_PRODUCTS_CATEGORIES:

                qs = get_another_multiple_product(product, cart_obj)

                # if there is available another product -> replace
                if qs:
                    cart_obj.products.remove(product)
                    new_product = qs.first()
                    cart_obj.products.add(new_product)
                    continue

            # remove not available product from cart
            all_available = False
            cart_obj.products.remove(product)
            request.session['cart_items_count'] = cart_obj.products.count()

            messages.warning(request, f"Produkt {product} został usunięty z karty")

    if all_available is True:
        if make_checkout:
            redirect_path += "?make_checkout=True"

        return redirect(redirect_path)

    else:
        messages.warning(request, f"Ktoś Cię ubiegł, jeden z wybranych przez Ciebie produktów jest już niedostępny :(")
        return redirect("carts:home")


def send_order_success_mail(request, success_order_id, success_user_email, success_order_total, bought_products, order_obj):
    mail_subject = f"naklisze.pl - Potwierdzenie zamówienia nr {success_order_id}"
    message = render_to_string('carts/checkout_success_email.html', {
        'success_order_id': success_order_id,
        'success_user_email': success_user_email,
        'success_order_total': success_order_total,
        'bought_products': bought_products,
        'order': order_obj
    })
    email_msg = EmailMessage(
        mail_subject, message, to=[success_user_email, "naklisze@gmail.com"]
    )
    try:
        email_msg.send()
    except:
        messages.warning(request, f'Nastąpił błąd przy wysyłaniu potwierdzenia na adres {success_user_email}')
