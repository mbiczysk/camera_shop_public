from django.db import models
from django.conf import settings

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from .signals import object_viewed_signal
from .utils import get_client_ip
from products.models import Camera, Strap, Button

User = settings.AUTH_USER_MODEL

PRODUCTS_CLASSES = [Camera, Strap, Button]


class ObjectViewedManager(models.Manager):
    def viewed_products(self):
        c_types = [ ContentType.objects.get_for_model(model_class) for model_class in PRODUCTS_CLASSES ]
        return self.get_queryset().filter(content_type__in=c_types).order_by('-timestamp')


class ObjectViewed(models.Model):
    user            = models.ForeignKey(User, blank=True, null=True) # User instance
    ip_address      = models.CharField(max_length=200, blank=True, null=True)
    content_type    = models.ForeignKey(ContentType) # Product, Order, Cart, Address...
    object_id       = models.PositiveIntegerField() # UserId, ProductId, OrderId...
    content_object  = GenericForeignKey('content_type', 'object_id') # Product instance, ....
    timestamp       = models.DateTimeField(auto_now_add=True)

    objects = ObjectViewedManager()

    def __str__(self):
        return f"{self.user} viewed {self.content_object} on {self.timestamp}"


    class Meta:
        ordering = ['-timestamp'] # most recent showed first
        verbose_name = 'Object viewed'
        verbose_name_plural = 'Objects viewed'

def object_viewed_receiver(sender, instance, request, *args, **kwargs):
    c_type = ContentType.objects.get_for_model(sender)  #  same as instance.__class__

    if request.user.is_authenticated():
        user = request.user
    else:
        user = None

    new_view_obj = ObjectViewed.objects.create(
        user = user,
        content_type = c_type,
        ip_address = get_client_ip(request),
        object_id = instance.id

    )


object_viewed_signal.connect(object_viewed_receiver)