from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin

from orders.models import Order


class AnalyticsView(LoginRequiredMixin, TemplateView):
    template_name = 'analytics/view.html'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_staff:
            return HttpResponse(' ( . )( . ) ')
        return super(AnalyticsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(AnalyticsView, self).get_context_data(*args, **kwargs)
        orders_qs = Order.objects.all_confirmed()
        income = sum([order.total for order in orders_qs])

        context['orders'] = orders_qs
        context['income'] = income
        return context
