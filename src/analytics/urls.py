from django.conf.urls import url
from .views import AnalyticsView


urlpatterns = [
    url(r'^panel/$', AnalyticsView.as_view(), name='panel'),
]
