from django import forms
from .models import Address


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = [
            # 'billing_profile',
            # 'address_type',
            'company_name',
            'name',
            'surname',
            'street',
            'home_number',
            'postal_code',
            'city',
            'phone'
            ]

        widgets = {
            'name': forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "* Imię",
            }),
            'surname': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Nazwisko",
                }),
            'company_name': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "Nazwa firmy",
                }),
            'street': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Ulica",
                }),
            'home_number': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Nr domu/mieszkania",
                }),
            'postal_code': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Kod pocztowy",
                }),
            'city': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Miasto",
                }),
            'phone': forms.TextInput(
                attrs={
                    "class": "form-control",
                    "placeholder": "* Nr telefonu",
                }),

        }
        labels = {
            'name': 'Imię',
            'surname': 'Nazwisko',
            'company_name': 'Nazwa firmy',
            'street': 'Ulica',
            'home_number': 'Nr domu/mieszkania',
            'postal_code': 'Kod pocztowy',
            'city': 'Miasto',
            'phone': 'Nr telefonu',

        }
