from django.db import models
from billing.models import BillingProfile


ADDRESS_TYPES = (
    ('billing', 'Billing'),
    ('shipping', 'Shipping')
)

class Address(models.Model):
    billing_profile     = models.ForeignKey(BillingProfile)
    address_type        = models.CharField(max_length=60, choices=ADDRESS_TYPES)
    company_name        = models.CharField(max_length=120, null=True, blank=True)
    name                = models.CharField(max_length=60)
    surname             = models.CharField(max_length=60)
    street              = models.CharField(max_length=60)
    home_number         = models.CharField(max_length=12)
    postal_code         = models.CharField(max_length=12)
    city                = models.CharField(max_length=60)
    phone               = models.CharField(max_length=20)
    current             = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.id}_{str(self.billing_profile)}_{self.address_type}"

    def print_address_html(self):
        address_list = [
            f"{self.name} {self.surname} {self.company_name or ''}",
            f"ul. {self.street} {self.home_number}",
            f"{self.postal_code} {self.city}",
            f"tel. {self.phone}"
        ]
        return  address_list

    def print_address(self):
        return f"{self.name} {self.surname} {self.company_name or ''} \nul. {self.street} {self.home_number} \n{self.postal_code} {self.city} \ntel. {self.phone}"

    def make_stale(self):
        self.current = False
        self.save()