from django.views.generic import ListView
from products.models import Product
from carts.models import Cart


class SearchProductView(ListView):
    template_name = 'search/view.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(SearchProductView, self).get_context_data()
        context['query'] = self.request.GET.get('q')
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        query = request.GET.get('q')
        if query is not None:
            results = Product.objects.search(query)
            if results is not None:
                return results
        return Product.objects.featured()
