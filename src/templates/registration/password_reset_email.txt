{% load i18n %}
{% blocktrans %}
Witaj,

Otrzymaliśmy rządanie resetu hasła w serwisie {{ domain }} dla użytkownika {{ user }}
{% endblocktrans %}

{% block reset_link %}
Jeżeli to Ty, kliknij w poniższy link aby dokończyć procedurę:
{{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}

W przeciwnym przypadku prosimy o zignorowanie tej wiadomości.

Pozdrawiamy

naklisze.pl

{% endblock %}
