from django.db import models
from carts.models import Cart
from sklep.utils import uniqe_order_id_generator
from django.db.models.signals import pre_save, post_save
from billing.models import BillingProfile
from addresses.models import Address
from decimal import *


ORDER_STATUS_CHOICES = (
    ('created', 'Stworzone'),
    ('done', 'Zatwierdzone'),
    ('paid', 'Zapłacone'),
    ('shipped', 'Wysłane'),
    ('refunded', 'Zwrot')
)

ORDER_LOGGED_USER_PROMO = 5


class OrderManager(models.Manager):
    def new_or_get(self, cart_obj, billing_profile):
        created = False
        qs = self.get_queryset().filter(
            cart=cart_obj,
            billing_profile=billing_profile,
            active=True,
            status='created'
        )

        if qs.count() == 1:
            obj = qs.first()
        else:
            obj = self.model.objects.create(cart=cart_obj, billing_profile=billing_profile)
            created = True
        return obj, created

    def user_orders_history(self, user):
        qs = self.get_queryset().filter(
            billing_profile__email=user.email,).exclude(
                status='created').order_by('-timestamp')
        return qs

    def all_confirmed(self):
        qs = self.get_queryset().all().exclude(
            status='created').exclude(status='refunded').order_by('-timestamp')
        return qs


class Order(models.Model):
    order_id            = models.CharField(max_length=60, blank=True)
    timestamp           = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    billing_profile     = models.ForeignKey(BillingProfile, null=True, blank=True)
    shipping_address    = models.ForeignKey(Address, related_name="shipping_address", null=True, blank=True)
    billing_address     = models.ForeignKey(Address, related_name="billing_address", null=True, blank=True)
    cart                = models.ForeignKey(Cart)

    status              = models.CharField(max_length=20, default='created', choices=ORDER_STATUS_CHOICES)
    products_cost       = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    shipping_cost       = models.DecimalField(default=20.00, max_digits=10, decimal_places=2)
    total               = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    active              = models.BooleanField(default=True)
    logged_user_promo   = models.IntegerField(default=0)

    objects = OrderManager()

    def __str__(self):
        return self.order_id

    def update_logged_user_promo(self):
        if self.billing_profile.user is not None:
            self.logged_user_promo = ORDER_LOGGED_USER_PROMO

    def update_all(self):
        self.update_logged_user_promo()
        self.shipping_cost = self.cart.shipping_cost
        self.products_cost = self.cart.subtotal
        promotion_factor = ((Decimal(100) - self.logged_user_promo) / Decimal(100))
        self.total = self.cart.total * promotion_factor
        self.save()

    def check_if_done(self):
        billing_profile = self.billing_profile
        shipping_address = self.shipping_address
        billing_address = self.billing_address
        total = self.total
        if billing_profile and shipping_address and billing_address and total > 0:
            return True
        return False

    def mark_as_done(self):
        if self.check_if_done():
            self.status = 'done'
            self.save()
        return self.status

    def print_order(self):
        return f" Zamówienie nr: {self.order_id} \n Adres dostawy: {self.shipping_address} \n Status: {self.status} \n Całkowity koszt {self.total}"

    def print_order_html(self):
        order_list = [
            f"Data zamówienia: {self.timestamp.strftime('%Y-%m-%d')}",
            f"Adres dostawy: {self.shipping_address.print_address()}",
            f"Dane kupującego: {self.billing_address.print_address()}",
            f"Koszt produktów {self.products_cost} zł",
            f"Koszt przesyłki {self.shipping_cost} zł",
            f"Całkowity koszt {self.total} zł",
            f"Rabat {self.logged_user_promo} %",
            f"Status: {self.get_status_display()}"
        ]
        return  order_list

def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = uniqe_order_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
    if qs.exists():
        qs.update(active=False)


pre_save.connect(pre_save_create_order_id, sender=Order)

def post_save_cart_total(sender, instance, created,  *args, **kwargs):
    if not created:
        cart_obj = instance
        cart_id = cart_obj.id
        qs = Order.objects.filter(cart__id=cart_id, active=True)
        if qs.count() == 1:
            order_obj = qs.first()
            order_obj.update_all()

post_save.connect(post_save_cart_total, sender=Cart)

def post_save_order_total(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_all()

post_save.connect(post_save_order_total, sender=Order)
