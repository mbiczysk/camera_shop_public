from django.contrib import admin

from .models import Order

class OrderAdmin(admin.ModelAdmin):
    list_display = ['timestamp', '__str__', 'billing_profile', 'logged_user_promo', 'total', 'status']
    ordering = ['-status']

admin.site.register(Order, OrderAdmin)
