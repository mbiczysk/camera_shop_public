from django.contrib import admin
from django.core.urlresolvers import resolve

from .models import Product, Camera, Strap, Button, ProductImage, CameraSample

from polymorphic.admin import (
    PolymorphicParentModelAdmin,
    PolymorphicChildModelAdmin,
    PolymorphicChildModelFilter,
)


def make_all_available(modeladmin, request, queryset):
    queryset.update(selled=False)
    queryset.update(active=True)

make_all_available.short_description = "Mark selected products as active and available"


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 5

class CameraSampleInline(admin.TabularInline):
    model = CameraSample


class ProductChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Product  # Optional, explicitly set here.
    readonly_fields = [
        'title',
        'slug',
    ]

    # By using these `base_...` attributes instead of the regular ModelAdmin `form` and `fieldsets`,
    # the additional fields of the child models are automatically added to the admin form.
    """
    base_form = ...
    base_fieldsets = (
        ...
    )
    """

    inlines = (ProductImageInline,)
    save_as = True
    # Copy ProductImage inlines to new object when using 'save as' button
    # double save() method to avoid error : 'save() prohibited to prevent data loss due to unsaved related object'
    def save_model(self, request, obj, form, change):
        # Django always sends this when "Save as new is clicked"
        obj.save()
        if '_saveasnew' in request.POST:
            # Get the ID from the admin URL
            original_pk = resolve(request.path).args[0]
            # Get the original object
            original_obj = obj._meta.concrete_model.objects.get(id=original_pk)

            # Create ProductImage objects with foreign key to new copied Product object
            original_images = ProductImage.objects.filter(product=original_obj)
            for image in original_images:
                ProductImage.objects.create(product=obj, image=image.image)
        obj.save()


@admin.register(Camera)
class CameraAdmin(ProductChildAdmin):
    base_model = Camera  # Explicitly set here!
    # show_in_index = True  # makes child model admin visible in main admin site
    # define custom features here
    fields = (
        'title',
        'category',
        'brand',
        'model',
        'active',
        'selled',
        'price',
        'promo_price',
        'promo_active',
        'featured',
        'serial_number',
        'quality',
        'production_date',
        'description',
        'spec_table',
        'set_description',
        'tags',
        'slug',
    )

    inlines = (ProductImageInline, CameraSampleInline,)


@admin.register(Strap)
class StrapAdmin(ProductChildAdmin):
    base_model = Strap
    fields = (
        'title',
        'type',
        'model',
        'active',
        'selled',
        'price',
        'promo_price',
        'promo_active',
        'featured',
        'description',
        'tags',
        'slug',
        'category',
    )


@admin.register(Button)
class ButtonAdmin(ProductChildAdmin):
    base_model = Button
    fields = (
        'title',
        'model',
        'active',
        'selled',
        'price',
        'promo_price',
        'promo_active',
        'featured',
        'description',
        'tags',
        'category',
        'slug',
    )



@admin.register(Product)
class ProductParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Product  # Optional, explicitly set here.
    child_models = (
        Camera,
        Strap,
        Button,
    )

    list_filter = (PolymorphicChildModelFilter,)
    list_display = ('title', 'category', 'active', 'selled', 'featured', 'price', 'promo_price', 'promo_active',)
    actions = [make_all_available]

