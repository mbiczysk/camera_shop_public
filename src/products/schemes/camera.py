CAMERA_DEFAULT_DESCRIPTION = """
A camera is an optical instrument for recording or capturing images, which may be stored locally, transmitted to another location, or both. The images may be individual still photographs or sequences of images constituting videos or movies. The camera is a remote sensing device as it senses subjects without any contact . The word camera comes from camera obscura, which means "dark chamber" and is the Latin name of the original device for projecting an image of external reality onto a flat surface. The modern photographic camera evolved from the camera obscura. The functioning of the camera is very similar to the functioning of the human eye. The first permanent photograph of a camera image was made in 1826 by Joseph Nicéphore Niépce.
"""

DEFAULT_SPEC_TABLE = """
ogniskowa @ 58 mm &
kąt widzenia @ 32' szerokości &
parametr @ wartość &
kolejny parametr @ następna wartość &
"""

DEFAULT_SET_DESCRIPTION = """
aparat &
pokrowiec neoprenowy &
oryginalny pokrowiec &
"""