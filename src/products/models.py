import os
import random

from django.db import models
from django.db.models.signals import post_save
from django.db.models import Q

from django.urls import reverse

from polymorphic.models import PolymorphicModel
from polymorphic.managers import PolymorphicManager
from polymorphic.query import  PolymorphicQuerySet

from sklep.utils import unique_slug_generator

from .schemes.button import BUTTON_MODEL_CHOICES, BUTTON_DESCRIPTION
from .schemes.strap import STRAP_DESCRIPTION, STRAP_MODEL_CHOICES, STRAP_TYPE_CHOICES
from .schemes.camera import CAMERA_DEFAULT_DESCRIPTION, DEFAULT_SPEC_TABLE, DEFAULT_SET_DESCRIPTION


CATEGORIES_CHOICES = (
    ('range', 'Aparaty dalmierzowe'),
    ('half', 'Aparaty połówkowe'),
    ('compact', 'Aparaty kompaktowe'),
    ('medium', 'Aparaty średnioformatowe'),
    ('straps', 'Paski'),
    ('buttons', 'Przyciski spustu')
)

DICT_CATEGORIES_CHOICES = dict(CATEGORIES_CHOICES)


def split_filename(filename):
    base_name = os.path.basename(filename)
    name, ext = os.path.splitext(base_name)
    return name, ext

def upload_image_path(instance, filename):
    new_filename = random.randint(1, 999999)
    name, ext = split_filename(filename)
    final_filename = f"{new_filename}{ext}"
    return f"products/{instance.product.slug}/{final_filename}"

class ProductQuerySet(PolymorphicQuerySet):
    def featured(self):
        return self.filter(featured=True)

    def active(self):
        return self.filter(active=True)

    def available(self):
        return self.filter(selled=False)

    def promoted(self):
        return self.filter(promo_active=True, promo_price__gt=0)

    def search(self, query):
        lookups = Q(title__icontains=query) | Q(tags__icontains=query)
        return self.filter(lookups).distinct()


class ProductManager(PolymorphicManager):
    def all(self):
        return self.get_queryset()

    def all_active(self):
        return self.get_queryset().active()

    def all_available(self):
        return self.get_queryset().active().available()

    def all_promo(self):
        return self.all_available().promoted()

    def latest(self):
        qs = self.all_available().order_by('-timestamp')[:6]
        return qs

    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id)
        if qs.count() == 1:
            return qs.first()
        else:
            return None

    def featured(self): # Product.object.featured()  -- usage example
        return self.all_available().filter(featured=True)

    def search(self, query):
        return self.all_available().search(query)


class Product(PolymorphicModel):
    title           = models.CharField(null=True, blank=True, max_length=120)
    slug            = models.SlugField(null=True, blank=True, unique=True)
    price           = models.DecimalField(decimal_places=2, max_digits=8, default=499.99)
    promo_price     = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True)
    promo_active    = models.BooleanField(default=False)
    featured        = models.BooleanField(default=False)
    active          = models.BooleanField(default=True)
    timestamp       = models.DateTimeField(auto_now_add=True)
    tags            = models.TextField(blank=True)
    selled          = models.BooleanField(default=False)
    category        = models.CharField(max_length=120, null=True, blank=True, choices=CATEGORIES_CHOICES)

    CAMERA_CATEGORIES = ['range', 'half', 'compact', 'medium']
    MULTIPLE_PRODUCTS_CATEGORIES = ['straps', 'buttons']

    objects = ProductManager()

    @staticmethod
    def make_qs_unique(qs):
        # sqllite limitation in distinct per model field workaround
        unique_items, item_model = [], []
        for item in qs:
            if item.model not in item_model:
                unique_items.append(item)
                item_model.append(item.model)
        return unique_items


    def get_absolute_url(self):
        return reverse('products:detail', kwargs={'slug': self.slug})

    def get_category_url(self):
        if self.category in self.CAMERA_CATEGORIES:
            return reverse('products:list', kwargs={'category': self.category})
        elif self.category in self.MULTIPLE_PRODUCTS_CATEGORIES:
            return reverse('products:multiple_list', kwargs={'category': self.category})
        else:
            return "/products/offer/"

    def get_category_name(self):
        return self.get_category_display()

    def make_inactive(self):
        self.active = False
        self.save()

    def make_selled(self):
        self.selled = True
        self.save()

    def is_camera(self):
        return isinstance(self, Camera)

    def is_multiple(self):
        return (self.category in self.MULTIPLE_PRODUCTS_CATEGORIES)

    def title_image(self):
        self.check_or_create_default_image()
        return self.images.all()[0].image.url

    def all_images(self):
        self.check_or_create_default_image()
        return [obj.image.url for obj in self.images.all()]

    def check_or_create_default_image(self):
        try:
            img = self.images.all()[0]
        except IndexError:
            ProductImage.create_default(self)


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to=upload_image_path, default='products/default.jpg')

    @staticmethod
    def create_default(product_obj):
        product_images = ProductImage.objects.filter(product=product_obj)
        if not product_images:
            ProductImage.objects.create(product=product_obj)


class Camera(Product):
    brand           = models.CharField(max_length=60, blank=True, null=True)
    model           = models.CharField(max_length=60, blank=True, null=True)
    serial_number   = models.CharField(max_length=20, blank=True, null=True, default='000001')
    quality         = models.IntegerField(null=True, blank=True)
    set_description = models.TextField(max_length=200, blank=True, null=True, default=DEFAULT_SET_DESCRIPTION)
    description     = models.TextField(default=CAMERA_DEFAULT_DESCRIPTION)
    spec_table      = models.TextField(max_length=300, blank=True, null=True, default=DEFAULT_SPEC_TABLE)
    production_date = models.CharField(max_length=20, blank=True, null=True, default=' - ')

    def __str__(self):
        return f"{self.brand} : {self.model}"

    def __init__(self, *args, **kwargs):
        super(Camera, self).__init__(*args, **kwargs)

    def make_title(self):
        self.title = f'{self.brand} {self.model}'
        self.save()

    def order_title(self):
        return f'Aparat {self.brand} {self.model} (no.{self.serial_number})'

    def tuple_spec_table(self):
        table = []
        pair = []
        curr = ''
        for word in self.spec_table.split():
            if word == '@':
                pair.append(curr)
                curr = ''
            elif word == '&':
                pair.append(curr)
                curr = ''
                table.append(pair)
                pair = []
            else:
                curr += word + ' '
        return table

    def set_items(self):
        table = []
        curr = ''
        for word in self.set_description.split():
            if word == '&':
                table.append(curr)
                curr = ''
            else:
                curr += word + ' '
        return table

    def all_samples(self):
        # return self.samples.all()[0].sample.url
        return [obj.sample.url for obj in self.samples.all()]


class CameraSample(models.Model):
    product = models.ForeignKey(Camera, related_name='samples', on_delete=models.CASCADE)
    sample = models.ImageField(upload_to=upload_image_path, null=True, blank=True, default='products/sample.jpg')


class Strap(Product):
    type        = models.CharField(max_length=120, null=True, blank=True, choices=STRAP_TYPE_CHOICES)
    model       = models.CharField(max_length=120, null=True, blank=True, choices=STRAP_MODEL_CHOICES)
    description = models.TextField(default=STRAP_DESCRIPTION)

    def __str__(self):
        return f"{self.get_type_display()} : {self.get_model_display()}"

    def make_title(self):
        self.title = f'Pasek {self.get_type_display()}'
        self.save()

    def set_category(self):
        self.category = 'straps'
        self.save()

    def print_attr(self):
        return f"Model: {self.get_model_display()}"

    def order_title(self):
        return f'{self.title} : {self.get_model_display()}'


class Button(Product):
    model       = models.CharField(max_length=120, null=True, blank=True, choices=BUTTON_MODEL_CHOICES)
    description = models.TextField(default=BUTTON_DESCRIPTION)

    def __str__(self):
        return f"{self.get_model_display()}"

    def set_category(self):
        self.category = 'buttons'
        self.save()

    def make_title(self):
        self.title = f'Przycisk spustu'
        self.save()

    def print_attr(self):
        return f"Kolor: {self.get_model_display()}"

    def order_title(self):
        return f'{self.title} : {self.print_attr()}'


def product_post_save_receiver(sender, instance, *args, **kwargs):

    if not instance.title:
        instance.make_title()

    if not instance.category:
        instance.set_category()

    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
        instance.save()


for subclass in Product.__subclasses__():
    post_save.connect(product_post_save_receiver, sender=subclass)
