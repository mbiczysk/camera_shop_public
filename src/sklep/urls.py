from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView

from django.conf import settings
from django.conf.urls.static import static

from .views import home_page, AboutPageView, cookies_policy, regulations_page


urlpatterns = [
    url(r'^$', home_page, name='home_page'),
    url(r'^about/$', AboutPageView.as_view(), name='about'),
    url(r'^analytics/', include("analytics.urls", namespace='analytics')),
    url(r'^regulations/$', regulations_page, name='regulations'),
    url(r'^cookies/$', cookies_policy, name='cookies'),
    url(r'^accounts/', include("accounts.urls", namespace='accounts')),
    url(r'^accounts/', include("accounts.passwords.urls")),
    url(r'^products/', include("products.urls", namespace='products')),
    url(r'^search/', include("search.urls", namespace='search')),
    url(r'^marketing/', include("marketing.urls", namespace='marketing')),
    url(r'^cart/', include("carts.urls", namespace='carts')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

