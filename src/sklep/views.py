from django.shortcuts import render
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError
from django.views.generic import FormView
from django.shortcuts import redirect


from .forms import ContactForm
from carts.models import Cart
from products.models import Product, Camera
from marketing.models import MarketingPref


def regulations_page(request):
    context = {
    }
    return render(request, 'regulations.html', context)


def home_page(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    promo_products = Product.objects.all_promo()
    featured_products = Product.make_qs_unique(Product.objects.featured())
    new_products = Camera.objects.latest()

    newsletter_visible = True


    if request.user.is_authenticated:
        qs = MarketingPref.objects.filter(user=request.user)
        if qs:
            marketing_obj = qs.first()
            if marketing_obj.subscribed:
                newsletter_visible = False

    context = {
        'cart': cart_obj,
        'promo_products': promo_products,
        'featured_products': featured_products,
        'new_products': new_products,
        'newsletter_visible': newsletter_visible
    }

    if request.user.is_authenticated():
        context['username'] = request.user.username

    return render(request, 'home_page.html', context)


class AboutPageView(FormView):
    template_name = 'about.html'
    form_class = ContactForm

    def form_valid(self, form):
        request = self.request
        fullname = form.cleaned_data['fullname']
        email = form.cleaned_data['email']
        message = form.cleaned_data['message']
        subject = "Wiadomość od {}".format(fullname)

        try:
            send_mail(subject, message, email, ['naklisze@gmail.com'])
            messages.success(request, 'Pomyślnie wysłano wiadomość')
            form = ContactForm(None)

        except BadHeaderError:
            messages.warning(request, 'Nastąpił błąd przy wysyłaniu wiadomości')

        return redirect('about')

    def form_invalid(self, form):
        request = self.request
        messages.warning(request, 'Formularz został wypełniony niepoprawnie!')
        return redirect('about')


def cookies_policy(request):
    context = {
    }
    return render(request, 'base/cookies_policy.html', context)