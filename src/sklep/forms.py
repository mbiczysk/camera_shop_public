from django import forms


class ContactForm(forms.Form):
    fullname = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": "Twoje imię i nazwisko",
            }
        ),
        label="Dane kontaktowe"
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                "class": "form-control",
                "placeholder": "Twój E-Mail",
            }
        ),
        label="E-Mail"
    )
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={
                "class": "form-control",
                "placeholder": "Treść wiadomości"
            }
        ),
        label="Treść wiadomości"
    )
